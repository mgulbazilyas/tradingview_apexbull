import os
import datetime
from webflowpy.Webflow import Webflow
import requests
from bs4 import BeautifulSoup as bs
import json

with open('output_template.html', 'r') as stream:
    template = stream.read()
# Initialize the API
webflow_api = Webflow('7ffc2f52b3116f711127e6d77949e9f2e3de01f089986f772bac89c91e06a0a9')
collection_id = '601a82464762ac018184cad3'
category_id = '601a7efd95dc4c1760925cf9'
site_id = '5eadac66a0d3e4aa06617c76'
date_format = '%Y-%m-%dT%H:%M:%S.000Z'
history_file = 'done_posts.txt'
pattern = r'config = ([\w\W\n ]+\}); if'
collections = {
    'category': '5ec45d47ea224066435c4c1d',  # name
    'insight': '5ec45dce4be42f8a824850fb',
    'tag': '5ec4d913fce92deff2a29972',
}

related_colection_name = 'category'
tag_collection_name = 'tag'
related_to = {i.get('name'): i.get('_id') for i in
              webflow_api.items(collection_id=collections.get(related_colection_name)).get('items')}
tags = {i.get('name'): i.get('_id') for i in
        webflow_api.items(collection_id=collections.get(tag_collection_name)).get('items')}

types = {
    'Education': '3a37e339cf00d53ce33d2293f4a45f0a',
    'Long': 'f9caa2303244aaf76297213b6f2927a3',
    'Short': '9888860f97c83c863794dd5485c351c4',
    'Other': 'e44d5c36b658ce8f8c69592d2ce31932',
}




def type_definer(row):
    try:
        return row.select_one('.tv-idea-label').text
    except:
        return 'Other'


def related_to_id(tag_title):
    if tag_title in related_to:
        return related_to[tag_title]
    else:
        # Create Category
        x = webflow_api.createItem(collection_id=collections.get(related_colection_name), item_data={
            'name': tag_title,
            '_archived': False,
            '_draft': False,
        }, live=True)
        cid = x.get('_id')
        related_to.update({tag_title: cid})
        return cid


def tag_id(tag_title):
    if tag_title in tags:
        return tags[tag_title]
    else:
        # Create Category
        x = webflow_api.createItem(collection_id=collections.get(tag_collection_name), item_data={
            'name': tag_title,
            '_archived': False,
            '_draft': False,
        }, live=True)
        cid = x.get('_id')
        tags.update({tag_title: cid})
        return cid


if os.path.exists(history_file):
    with open(history_file, 'r') as stream:
        done = stream.read().strip().split('\n')
else:
    done = []


def add_to_done(line):
    global done
    done.append(line)
    with open(history_file, 'a') as stream:
        stream.write(line)
        stream.write('\n')


def get_tags(url):
    # https://www.tradingview.com/chart/USDOLLAR/ye494nQI-Stock-Market-Update-SP500-Dow-Jones-NASDAQ-GOLD-USDOLLAR/
    
    response = requests.get(url)
    soup = bs(response.content)
    links = soup.select('.tv-chart-view__info-area--tags>a')
    t_tags = [i.text.strip() for i in links]
    tag_ids = [tag_id(tag) for tag in t_tags]
    return tag_ids

def get_data(query):
    api_url = 'https://www.tradingview.com/api/v1/user/profile/charts/'
    response = requests.get(api_url, query).json()
    
    soup = bs(response.get('html'))
    count = response.get('results').get('count')
    total = response.get('results').get('total')
    return soup, count, total
    


def add_data(main_url):
    
    response = requests.get(main_url)
    
    soup = bs(response.content)
    data_id = soup.select_one('.tv-profile__button[data-id]').attrs['data-id']
    offset = 0
    query_param = {
        'by': data_id,
        's': offset
    }
    
    soup, count, total = get_data(query_param)
    
    while offset < total:
    
        data = [dict(short_symbol=json.loads(i.attrs['data-widget-data']).get('short_symbol'),
                     name=json.loads(i.attrs['data-card']).get('data').get('name'),
                     url=json.loads(i.attrs['data-card']).get('data').get('published_url'),
                     description=str(i.select_one('.tv-widget-idea__description-row')),
                     chart_url=json.loads(i.attrs['data-card']).get('data').get('image_url'),
                     summary=i.select_one('.tv-widget-idea__description-row').text.strip().replace('\n', ''),
                     type=types[type_definer(i).strip()],
                     **{'date_posted': eval(i.select_one('[data-timestamp]').attrs['data-timestamp'])},
                     data_id=i.attrs['data-uid'],
                     _archived=False,
                     _draft=False,
                     ) for i in soup.select('[data-widget-type="idea"]')]
        
        for row in data:
            # row = data[0]
            # row['slug'] = slugify.slugify(row.get('name'))
            data_id = row.pop('data_id', '')
            if data_id in done:
                print(data_id, 'Already Done')
                break
            short_symbol = row.pop('short_symbol', '')
            date_posted = row.pop('date_posted', '')
            chart_url = row.pop('chart_url', '')
            url = row.pop('url', '')
            description = row.pop('description', '')
            row['content'] = template.format(chart_url=chart_url, url=url, title=row.get('name'), description=description)
            
            # TODO:
            '''
            # TODO:
            [
            {'slug': 'short_symbol','msg': 'Field not described in schema'},
          {'slug': 'date_posted', 'msg': 'Field not described in schema'},
          {'slug': 'data_id', 'msg': 'Field not described in schema'},
          {'slug': 'related-to', 'msg': 'Field is required', 'value': None},
          {'slug': 'tag', 'msg': 'Field is required', 'value': None}]
            '''
            row['link-to-chart'] = url
            row['date-posted'] = datetime.datetime.fromtimestamp(date_posted).strftime(date_format)
            row['related-to'] = related_to_id(short_symbol)
            # 6068841bc9754cdaead54373 in related to
            # 6068831f3c9213682ff04f26 in tag
            row['tags'] = []  # get_tags(url)
            # row['live'] = True
            # row['published-by'] = 'Person_5e81e2f4957e0ac5b335fc80'
            # row['published-on'] = datetime.datetime.fromtimestamp(date_posted).strftime(date_format)
            webflow_api.createItem(collection_id=collections.get('insight'), item_data=row, live=True)
            add_to_done(data_id)
        else:
            offset += count
            query_param['s'] =  offset
            

            soup, count, total = get_data(query_param)

            continue
        break

    # insights = webflow_api.items(collection_id=collections.get('insight')).get('items')
    return ''


if __name__ == '__main__':
    add_data('https://www.tradingview.com/u/ApexBull-ImperiumFX/')
    pass
